## This module creates the app and gets us up and running.

import gui

import argparse
import random
import time

parser = argparse.ArgumentParser()
parser.add_argument('--seed', dest = 'seed', default = int(time.time()))
parser.add_argument('--ui', dest='uimode', default = 'WX', choices = ['WX', 'CURSES', 'QT'])
args = parser.parse_args()

seed = args.seed
print "Using random seed",seed
random.seed(seed)

# Load data files.
import things.items.itemLoader
things.items.itemLoader.loadFiles()
import things.creatures.creatureLoader
things.creatures.creatureLoader.loadFiles()
import things.terrain.terrainLoader
things.terrain.terrainLoader.loadFiles()
import procs.procLoader
procs.procLoader.loadFiles()

# For now, create the player and game map here. This is clearly the wrong place,
# but we need to do it at some point, and for now we don't have the right
# point to do it, so we do it here.
import mapgen.gameMap
newMap = mapgen.gameMap.GameMap(360, 120)
import things.creatures.player
things.creatures.player.debugMakePlayer(newMap)
newMap.makeLevel(0)

gui.setUIMode(gui.__dict__[args.uimode])
gui.init(newMap)
