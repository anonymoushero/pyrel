# coding=utf-8
## Grammar utilities

## Generate the appropriate suffix for the given name based on its quantity.
def makePlural(basename, quantity):
    if quantity == 1:
        # \todo Figure out a better way to determine the plural suffix.
        # (can port from V for knives, staves etc.)
        return basename.replace(u'~', u'')
    return basename.replace(u'~', u's')


## Generate a grammatically-correct name for the given item description.
# \param name The base name of them item, with '&' a placeholder for the
#        article and '~' a placeholder for the suffix
# \param quantity The number of items (defaults to 1)
# \param flavor An adjective that needs to be applied to the item 
#        (e.g. "Red" in "Red Potion")
# \param ability An adjective phrase describing the item (e.g. "Slay Giant" or
#        "Cure Light Wounds). 
def getGrammaticalName(basename, quantity = 1, prefix = None, suffix = None):
    # set default return values
    article = unicode(quantity);
    name = makePlural(basename, quantity)

    # Add any prefix, preserving the article marker (&)
    if prefix is not None:
        if name.startswith(u'&'):
            name = name.replace(u'&', u'& %s' % prefix)
        else:
            name = u'%s %s' % (prefix, name)
            
    # Set the correct article
    if quantity == 0:
        article = u'no'
    elif quantity == 1:
        # \todo Assuming 'an' when the name starts with a vowel isn't 
        # perfectly reliable (c.f. "uranium").
        if name.replace(u'& ', u'')[0].lower() in englishVowels:
            article = u'an'
        else:
            article = u'a'
   
    if suffix is not None:
        # Insert the suffix at the end of the name.
        name += u' %s' % suffix

    # Apply the article
    return name.replace(u'&', article)

## Not all these letters are actually in the English language, but what with all the borrowed words...
englishVowels = u'aeiouàáâãäåæèéêëìíîïðòóôõöøùúûüāăąēĕėęěĩīĭįıĳōŏőœũūŭůűų'

