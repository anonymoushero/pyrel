## This module handles certain common colors.

import json
import os

COLOR_MAP = json.load(open(os.path.join('data', 'colors.txt'), 'r'))


## Given a color name, return the associated RGB tuple.
def getColorByName(name):
    if name in COLOR_MAP:
        return COLOR_MAP[name]
    raise RuntimeError("Unrecognized color name [%s]" % name)
