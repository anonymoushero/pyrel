## This module handles mapping flavor names to the colors for those names.

import json
import os
import random

colorRecords = json.load(open(os.path.join('data', 'flavor_colors.txt'), 'r'))
typeRecords = json.load(open(os.path.join('data', 'flavor_types.txt'), 'r'))

## Maps flavor names to colors for those flavors.
FLAVOR_COLOR_MAP = {}
## Maps flavor categories to lists of flavor names.
FLAVOR_CATEGORY_MAP = {}
## Maps item base types to lists of flavor names.
FLAVOR_TYPE_MAP = {}

for record in colorRecords:
    FLAVOR_CATEGORY_MAP[record['type']] = record['flavors'].keys()
    for key, value in record['flavors'].iteritems():
        FLAVOR_COLOR_MAP[key] = value
for key, value in typeRecords.iteritems():
    FLAVOR_TYPE_MAP[key] = value


def getColorNameForFlavor(flavor):
    if flavor not in FLAVOR_COLOR_MAP:
        raise RuntimeError("Unrecognized flavor name [%s]" % flavor)
    return FLAVOR_COLOR_MAP[flavor]


def getFlavorsForCategory(category):
    if category not in FLAVOR_CATEGORY_MAP:
        raise RuntimeError("Unrecognized flavor category [%s]" % category)
    return FLAVOR_CATEGORY_MAP[category]


## Select a random flavor for an item of the given type.
# \todo This is probably the wrong place to do this, since we need to always
# choose the same flavor for a given item type in a given game. 
def chooseFlavorForItemType(type):
    if type not in FLAVOR_TYPE_MAP:
        raise RuntimeError("No known flavors for items of type [%s]" % type)
    categories = FLAVOR_TYPE_MAP[type]
    allFlavors = []
    for category in categories:
        allFlavors.extend(getFlavorsForCategory(category))
    return random.choice(allFlavors)
