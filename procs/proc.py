## Procs are, more or less, functions that we invoke at different times in the
# game engine, but which are inserted by the game data. 
# We make classes out of them because they may need to pre-process their 
# arguments (e.g. ensuring that an equipment bonus is consistent).

import gui
import things.stats
import things.synthetics.timer
import things.terrain.terrainLoader
import util.randomizer
import util.boostedDie



## A Proc is a bit of code that can be invoked at specific trigger conditions,
# as specified by some entity defined in the data files. In other words, when
# the game engine reaches certain conditions (e.g. an item is used, or a 
# monster is hit), the relevant entities are consulted to see if they have
# any Procs associated with the condition. If they do, those Procs are invoked.
class Proc:
    ## Create the Proc.
    # \param triggerCondition When the Proc is invoked.
    # \param params Maps parameter names to values for those parameters. 
    # Parameter interpretation is left up to the specific proc.
    # \param level The level of the effect -- may affect interpretation of 
    #        some of the parameters.
    def __init__(self, triggerCondition, params, procLevel):
        self.triggerCondition = triggerCondition
        self.params = params
        self.procLevel = procLevel


    ## Trigger the Proc, with any appropriate parameters.
    def trigger(self, *args):
        raise RuntimeError("Proc of type [%s] didn't implement its trigger function." % type(self))



## Prints a message when used.
class MessageProc(Proc):
    ## HACK: we allow this proc to be called with any variety of parameters,
    # since we don't care what its source was -- item, terrain, spell, etc.
    def trigger(self, *args, **kwargs):
        gui.messenger.message(self.params['messageString'])
        return True



## Prints a message that performs some string substitutions.
# HACK: requires the user to know which order the parameters are called in.
# This is really quite ugly.
class SmartMessageProc(Proc):
    def trigger(self, *args):
        string = self.params['messageString']
        for i, arg in enumerate(args):
            string = string.replace('<%d>' % (i + 1), str(arg))
        gui.messenger.message(string)
        return True



## Reduce the quantity of the invoked item.
class ReduceQuantityProc(Proc):
    def trigger(self, item, user, gameMap):
        item.quantity -= 1
        if item.quantity == 0:
            # No more item.
            gui.messenger.message("You have no more",item)
            gameMap.destroy(item)
            return True
        elif item.quantity < 0:
            raise RuntimeError("Item quantity less than 0; how did this happen?")
            return False



class TunnelProc(Proc):
    def trigger(self, terrain, tunneler, gameMap, pos):
        if util.randomizer.oneIn(self.params['difficulty']):
            gameMap.removeFrom(terrain, pos)
            gui.messenger.message(tunneler, "tunneled through the",terrain.name)
            return True
        else:
            gui.messenger.message(tunneler, "failed to tunnel through the",terrain.name,"(odds %d)" % (self.params['difficulty']))
            return False



## Replace the source terrain with a new terrain object.
class ReplaceTerrainProc(Proc):
    def trigger(self, terrain, actor, gameMap, pos):
        newTerrain = things.terrain.terrainLoader.makeTerrain(
                self.params['newTerrain'], 
                gameMap, terrain.pos, terrain.mapLevel)
        gameMap.removeFrom(terrain, pos)
        return True
       


## Apply a temporary stat modifier to the user.
class TemporaryStatModProc(Proc):
    def trigger(self, item, user, gameMap, *args, **kwargs):
        # \todo Picking an arbitrary high tier here (the 100).
        newMod = things.stats.StatMod(100, addend = self.params['modAmount'])
        user.addStatMod(self.params['statName'], newMod)
        # Ask the game to call us back when we expire, so we can remove the 
        # modifier we created.
        # \todo Allow scaled durations based on level -- what exactly should
        # level be?
        duration = util.boostedDie.roll(self.params['duration'])
        things.synthetics.timer.Timer(gameMap, duration, 
                lambda: user.removeStatMod(self.params['statName'], newMod))



## Generate a new level.
class ChangeLevelProc(Proc):
    def trigger(self, terrain, actor, gameMap, pos):
        targetLevel = terrain.mapLevel
        if self.params['newLevel'][0] in ('+', '-'):
            # Apply an offset
            targetLevel += int(self.params['newLevel'])
        else:
            # Move to a specific level.
            targetLevel = int(self.params['newLevel'])
        gameMap.makeLevel(targetLevel)



# These imports must be delayed until the Proc class is defined, but we don't
# actually need them until this point anyway.
import filterProc
import calculatorProc


## Mapping of proc names to the functions that invoke them. Normally this would
# go at the top of the file, but of course none of these symbols are defined
# at that point.
PROC_NAME_MAP = {
        "change level": ChangeLevelProc,
        "print message": MessageProc,
        "print smart message": SmartMessageProc,
        "reduce quantity": ReduceQuantityProc,
        "replace terrain": ReplaceTerrainProc,
        "temporary stat mod": TemporaryStatModProc,
        "tunnel": TunnelProc,
        # Following procs are filters.
        "don't allocate if already dead": filterProc.AllocationPreventionDeathClauseFilter,
        "don't allocate if already existing": filterProc.AllocationPreventionDoubleClauseFilter,
        "stop": filterProc.FalseFilter,
        # Following procs are calculators.
        "percentage stat mod": calculatorProc.PercentageStatModCalculator,
        "combining stat mod": calculatorProc.LinearCombinationStatModCalculator,
}


## If a Proc has one of these triggers, then it's a "static" Proc, i.e.
# not specific to an instantiated Thing, but rather associated with all 
# Things of that type/subtype. Generally this is needed when we need to make
# decisions about that class of Thing but don't an instanced one handy.
# NOTE: these procs are assumed to only exist to modify game logic, not to 
# have any effect on the game world. There is no guarantee that a given 
# Proc using one of these triggers will be invoked, if an earlier Proc
# with the same trigger returned False. 
STATIC_TRIGGERS = set([
    "allocation table creation",
    "allocation table selection",
])

