## This module handles subscription to and publishing of events. An event
# is simply a notification that something has happened.


## Maps event strings to sets of functions to call when those events occur.
eventTypeToFunctions = {}

## Subscribe to an event. When the event occurs, the provided function will
# be called.
def subscribe(eventType, func):
    if eventType not in eventTypeToFunctions:
        eventTypeToFunctions[eventType] = set()
    eventTypeToFunctions[eventType].add(func)


## Unsubscribe from an event, so that the specified function is no longer
# called when that event occurs.
def unsubscribe(eventType, func):
    eventTypeToFunctions[eventType].remove(func)


## Publish an event. Call all subscribed functions, passing along all event
# data.
def publish(eventType, *eventData):
    for func in eventTypeToFunctions.get(eventType, []):
        func(*eventData)

