## The player is, for now, just another Creature, only going into special
# containers.

import container
import creature

class Player(creature.Creature):
    def __init__(self, gameMap, pos, name):
        creature.Creature.__init__(self, gameMap, pos, name)
        self.resubscribe(gameMap)
        ## This is set to True if the user ever uses the debug commands, 
        # which are cheating.
        self.hasUsedDebugCommands = False


    def resubscribe(self, gameMap):
        creature.Creature.resubscribe(self, gameMap)
        gameMap.addSubscriber(self, container.PLAYERS)
        gameMap.addSubscriber(self, container.PERSISTENT)


    ## Just do nothing -- our state updates are handled by the commands
    # package.
    def update(self):
        pass


## Generate a test player.
def debugMakePlayer(gameMap):
    import creatureLoader
    player = Player(gameMap, (0, 0), '<player>')
    creatureLoader.getTemplate('Half-Troll').applyAsTemplate(player)
    creatureLoader.getTemplate('Warrior').applyAsTemplate(player)
    creatureLoader.getFactory('<player>').applyAsTemplate(player)
    player.subtype = '<player>'
    player.stats.roll(0)
    import things.stats
    player.stats.addMod('creatureLevel', things.stats.StatMod(0, 50))

