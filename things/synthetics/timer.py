import container
import things.thing
import things.mixins.updater

## Auto-incrementing ID used to generate timer names.
globalId = 0

## A Timer is an entity that waits a certain number of normal-speed turns, 
# calls a function, and then dies.
class Timer(things.thing.Thing, things.mixins.updater.Updateable):
    ## \param duration How many normal-speed turns to wait.
    # \param func Function to call when our time runs out.
    def __init__(self, gameMap, duration, func):
        things.thing.Thing.__init__(self)
        global globalId
        things.mixins.updater.Updateable.__init__(self, gameMap, 
                name = globalId, speed = 1, energy = 0)
        globalId += 1
        self.gameMap = gameMap
        self.duration = duration
        self.func = func
        self.numTurnsPassed = 0


    def update(self, *args):
        self.numTurnsPassed += 1
        self.energy = 0
        if self.numTurnsPassed >= self.duration:
            self.func()
            self.gameMap.destroy(self)

