## Load item records from object.txt and object_template.txt, and create
# ItemFactories for them.

import allocatorRules
import itemFactory
import affix
import loot
import util.record
import theme

import copy
import json
import os


## A list of the major item types
ITEM_TYPES = []
## A map from major item types to the subtypes available for each major type
# Lazily constructed.
ITEM_SUBTYPE_MAP = dict()
## A list of all the artifacts
ARTIFACTS = []

## Maps (type, subtype) tuples, or artifact names, to the factories
# needed to instantiate them.
ITEM_FACTORY_MAP = dict()

## Instantiate an Item, rolling all necessary dice to randomize it.
def makeItem(key, itemLevel, gameMap, pos = None):
    if key not in ITEM_FACTORY_MAP:
        raise RuntimeError("Invalid item key %s" % str(key))
    return ITEM_FACTORY_MAP[key].makeItem(itemLevel, gameMap, pos)


## Directly access the factory with the given tuple or name
def getFactory(key):
    if key not in ITEM_FACTORY_MAP:
        raise RuntimeError("Invalid item tuple or artifact name %s" % key)
    return ITEM_FACTORY_MAP[key]


## Get a list of the item types, and cache it for future calls
def getTypes():
    global ITEM_TYPES
    if not ITEM_TYPES:
        uniqueTypes = set()
        for key in ITEM_FACTORY_MAP:
            if type(key) == tuple:
                (itemType, subType) = key
                uniqueTypes.add(itemType)
        ITEM_TYPES = list(uniqueTypes)
    return ITEM_TYPES


## Get a list of the available subtypes for a particular major type
# and cache the result for later use.
def getSubTypes(itemType):
    if not itemType in ITEM_SUBTYPE_MAP:
        ITEM_SUBTYPE_MAP[itemType] = []
        for key in ITEM_FACTORY_MAP:
            if type(key) is tuple and key[0] == itemType:
                ITEM_SUBTYPE_MAP[itemType].extend(key[1])
    return ITEM_SUBTYPE_MAP[itemType]


## Get a list of all the artifacts
# This is essentially all the factories that are keyed by name
def getArtifacts():
    global ARTIFACTS
    if not ARTIFACTS:
        ARTIFACTS = [key for key in ITEM_FACTORY_MAP if type(key) != tuple]
    return ARTIFACTS


## Maps template names to the factories that can apply them.
TEMPLATE_NAME_MAP = dict()
def getItemTemplate(name):
    if name not in TEMPLATE_NAME_MAP:
        raise RuntimeError("Invalid item template: [%s]" % str(name))
    return TEMPLATE_NAME_MAP[name]


## Maps affix names to the objects that can apply them.
AFFIX_NAME_MAP = dict()
def getAffix(name):
    if name not in AFFIX_NAME_MAP:
        raise RuntimeError("Invalid item affix: [%s]" % str(name))
    return AFFIX_NAME_MAP[name]


## Maps theme names to the objects that can apply them.
THEME_NAME_MAP = dict()
def getTheme(name):
    if name not in THEME_NAME_MAP:
        raise RuntimeError("Invalid item theme: [%s]" % str(name))
    return THEME_NAME_MAP[name]


## Maps loot template names to the factories that can apply them.
LOOT_TEMPLATE_MAP = dict()
def getLootTemplate(name):
    if name not in LOOT_TEMPLATE_MAP:
        raise RuntimeError("Invalid loot template: [%s]" % str(name))
    # Return a copy of the template to allow resolution of formulae
    return copy.deepcopy(LOOT_TEMPLATE_MAP[name])


## Returns affix minima and maxima for a given itemLevel, with caching
AFFIX_LIMITS = dict()
def getAffixLimits(itemLevel):
    # Check if we have already cached the results for this itemLevel
    if itemLevel not in AFFIX_LIMITS:
        AFFIX_LIMITS[itemLevel] = dict()
        AFFIX_LIMITS[itemLevel]['levelsMin'] = dict()
        AFFIX_LIMITS[itemLevel]['levelsMax'] = dict()
        AFFIX_LIMITS[itemLevel]['typesMin'] = dict()
        AFFIX_LIMITS[itemLevel]['typesMax'] = dict()
        # Iterate over all affix levels, check whether any have minima or
        # maxima at this itemLevel
        for key, level in AFFIX_LEVELS.iteritems():
            for allocatorRule in level['allocatorRules']:
                # Note that -1 means "no maximum"
                if (itemLevel >= allocatorRule['minDepth'] and
                        (itemLevel <= allocatorRule['maxDepth'] or
                        allocatorRule['maxDepth'] is -1)):
                    # 0 is irrelevant as a minimum so we ignore it ...
                    if allocatorRule['minNum'] > 0:
                        AFFIX_LIMITS[itemLevel]['levelsMin'][key] = allocatorRule['minNum']
                    # ... but it's a relevant maximum, so we don't
                    if allocatorRule['maxNum'] >= 0:
                        AFFIX_LIMITS[itemLevel]['levelsMax'][key] = allocatorRule['minNum']
        # Now do the same for affix types
        for key, affixType in AFFIX_TYPES.iteritems():
            for allocatorRule in affixType['allocatorRules']:
                if (itemLevel >= allocatorRule['minDepth'] and
                        (itemLevel <= allocatorRule['maxDepth'] or
                        allocatorRule['maxDepth'] is -1)):
                    if allocatorRule['minNum'] > 0:
                        AFFIX_LIMITS[itemLevel]['typesMin'][key] = allocatorRule['minNum']
                    if allocatorRule['maxNum'] >= 0:
                        AFFIX_LIMITS[itemLevel]['typesMax'][key] = allocatorRule['maxNum']
    # Return a copy so the caller can modify without affecting the cache
    return allocatorRules.AffixLimits(AFFIX_LIMITS[itemLevel])


## Load the data files we need.
def loadFiles():
    ## First load templates, so they're available when we load objects.
    templates = util.record.loadRecords(os.path.join('data', 'object',
            'object_template.txt'), itemFactory.ItemFactory)
    for template in templates:
        TEMPLATE_NAME_MAP[template.templateName] = template

    # Now load the object records.
    items = util.record.loadRecords(
            os.path.join('data', 'object', 'object.txt'), 
            itemFactory.ItemFactory)
    for item in items:
        ITEM_FACTORY_MAP[(item.type, item.subtype)] = item

    # Load the object flag metadata.
    OBJECT_FLAGS = json.load(open(os.path.join('data', 'object',
            'object_flags.txt')))

    # Now load the affixes.
    affixes = util.record.loadRecords(
            os.path.join('data', 'object', 'affix.txt'),
            affix.Affix)
    for newAffix in affixes:
        AFFIX_NAME_MAP[newAffix.name] = newAffix

    # Load the affix metadata.
    AFFIX_LEVELS, AFFIX_TYPES = json.load(open(os.path.join('data', 'object',
            'affix_meta.txt')))

    # Now load the themes.
    themes = util.record.loadRecords(
            os.path.join('data', 'object', 'theme.txt'),
            theme.Theme)
    for newTheme in themes:
        THEME_NAME_MAP[newTheme.name] = newTheme

    # Now load the artifacts, adding them to the base item map by name
    artifacts = util.record.loadRecords(os.path.join('data', 'object',
            'artifact.txt'), itemFactory.ItemFactory)
    for artifact in artifacts:
        ITEM_FACTORY_MAP[artifact.nameSuffix] = artifact

    # Load the loot templates.
    lootTemplates = util.record.loadRecords(os.path.join('data', 'object',
            'loot_template.txt'), loot.LootTemplate)
    for template in lootTemplates:
        LOOT_TEMPLATE_MAP[template.templateName] = template

