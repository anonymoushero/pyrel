import container

## This class allows Things to pick up, carry, and drop items.
class Carrier:
    ## \param numSlots How many unique items the Thing can carry. 
    # \todo Pick a better default?
    def __init__(self, numSlots = 23, maxCount = None):
        ## Holds items we're carrying.
        self.inventory = container.Container()
        ## Max size of inventory, in terms of slots (i.e. unique item types)
        self.maxCarriedSlots = numSlots
        ## Max size of inventory, in terms of total individual items.
        self.maxCarriedCount = None
        ## Maps item types to sets of item subtypes that we can hold. If 
        # this is None, then we can hold anything; if an item type maps to 
        # None, then we can hold all subtypes of that type.
        self.typeToCarriableSubtypes = None
        ## Functions to trigger when we pick up / drop items.
        self.inventoryTriggers = set()


    ## Add us back to any appropriate Containers in the GameMap.
    def resubscribe(self, gameMap):
        if self.maxCarriedCount or self.maxCarriedSlots:
            gameMap.addSubscriber(self, container.CARRIERS)


    ## Return true if there's room for a specific Item in our inventory, and
    # it's of a type we can carry.
    # \todo Handle stack merging.
    def getCanCarryItem(self, item):
        # Paranoia: don't put things into themselves.
        if item is self:
            return False
        # Check typing.
        if self.typeToCarriableSubtypes:
            if item.type not in self.typeToCarriableSubtypes:
                # Can't carry anything of this type.
                return False
            if (self.typeToCarriableSubtypes[item.type] is not None and 
                    item.subtype not in self.typeToCarriableSubtypes[item.type]):
                # Can't carry anything of this subtype.
                return False
        if (self.maxCarriedSlots is not None and 
                len(self.inventory) >= self.maxCarriedSlots):
            # All slots are occupied.
            return False
        if (self.maxCarriedCount is not None and 
                self.getNumberCarriedItems() + item.quantity >= self.maxCarriedCount):
            # We've exceeded our max count of items.
            return False
        return True


    ## Get the total number of items we are carrying.
    def getNumberCarriedItems(self):
        return sum([i.quantity for i in self.inventory])


    ## Add an Item to our inventory, if possible; otherwise drop it.
    def addItemToInventory(self, item):
        if len(self.inventory) >= self.maxCarriedSlots:
            self.dropItem(item)
            item.onDrop(self, self.gameMap)
        else:
            self.inventory.subscribe(item)
            item.pos = None
            item.onPickup(self, self.gameMap)
            for trigger in self.inventoryTriggers:
                trigger(self, item, False)


    ## Remove an Item from our inventory (either to move it elsewhere or
    # because it's been destroyed). The caller is responsible for where it ends
    # up afterwards. We also recurse through our children if they are
    # containers. 
    # Return True if we are successful.
    def removeItemFromInventory(self, target):
        didSucceed = False
        if target in self.inventory:
            self.inventory.unsubscribe(target)
            didSucceed = True
        else:
            for item in self.inventory:
                if item.isContainer() and item.removeItemFromInventory(target):
                    didSucceed = True
        if didSucceed:
            for trigger in self.inventoryTriggers:
                trigger(self, target, True)
        return didSucceed


    ## Attach a function to our inventory, that gets called whenever an item
    # enters or leaves it.
    def addInventoryTrigger(self, func):
        self.inventoryTriggers.add(func)
        # Retroactively apply the function to all inventory items.
        for item in self.inventory:
            func(self, item, False)
            if item.isContainer():
                item.addInventoryTrigger(func)


    ## Remove a function from our inventory triggers.
    def removeInventoryTrigger(self, func):
        self.inventoryTriggers.remove(func)
        # Let the function clean up any changes it might have made.
        for item in self.inventory:
            func(self, item, True)
            if item.isContainer():
                item.removeInventoryTrigger(func)


    ## Drop an item, removing it from our inventory.
    def dropItem(self, item):
        self.removeItemFromInventory(item)
        item.pos = self.pos
        self.gameMap.addSubscriber(item, item.pos)
        item.onDrop(self, self.gameMap)


    ## Return True if we contain the specified item, or a container we contain
    # contains it, etc.
    def contains(self, target):
        for item in self.inventory:
            if item is target or (item.isContainer() and item.contains(target)):
                return True
        return False


    ## Return True if any item in ourselves matches the specified container key.
    def containsMatch(self, key):
        if self.gameMap.filterContainer(self.inventory, key):
            return True
        for item in self.inventory:
            if item.isContainer() and item.containsMatch(key):
                return True
        return False


    ## Get how many slots of items are in the inventory.
    def getNumUsedSlots(self):
        return len(self.inventory)


    ## Get how many total items are in the inventory.
    def getNumContainedItems(self):
        return sum([i.quantity for i in self.inventory])



## Yield items from the provided container. When we encounter a container, we
# recurse (if recursionDepth allows), yielding a tuple of 
# (parent item, child item). Of course this tuple may achieve arbitrary 
# length depending on recursionDepth.
# \param recursionDepth If we are carrying other containers, and
# recursionLevel is at least 1, and those containers are open, then we list
# their contents too.
def generateItemList(inventory, recursionDepth = 2):
    for item in inventory:
        yield (item,)
        if item.isContainer() and item.isOpen() and recursionDepth > 1:
            for subItem in generateItemList(item.inventory, recursionDepth - 1):
                yield (item,) + subItem


## Index into a provided container, with recursion, like in generateItemList.
def indexIntoInventory(inventory, index, recursionDepth = 2, curCount = 0):
    amTopLevel = curCount == 0
    for item in inventory:
        if curCount == index:
            return item
        curCount += 1
        if item.isContainer() and item.isOpen() and recursionDepth > 1:
            result = indexIntoInventory(item.inventory, index, 
                    recursionDepth - 1, curCount)
            if type(result) is int:
                # No result; just update curCount
                curCount = result
            else:
                return result
    if amTopLevel:
        # No item exists at that index.
        return None
    # Otherwise we're in one of the recursion layers; return curCount so that
    # our caller knows how many items we've iterated over.
    return curCount

